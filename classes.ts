interface UserInterface {
  name: string;
  email: string;
  age: number;
  register();
  payInvoice();
}

class User implements UserInterface {
  name: string;
  email: string;
  age: number;

  constructor(name: string, email: string, age: number) {
    this.name = name;
    this.email = email;
    this.age = age;
  }

  register() {
    console.log(this.name + " is now registered");
  }

  payInvoice() {
    console.log(this.name + " paid invoice");
  }
}

class Memeber extends User {
  id: number;

  constructor(id: number, name: string, email: string, age: number) {
    super(name, email, age);
    this.id = id;
  }

  payInvoice() {
    super.payInvoice();
  }
}

let member: User = new Memeber(1, "John", "john@gmail.com", 34);
member.register();
member.payInvoice();
