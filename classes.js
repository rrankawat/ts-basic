var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var User = (function () {
    function User(name, email, age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }
    User.prototype.register = function () {
        console.log(this.name + " is now registered");
    };
    User.prototype.payInvoice = function () {
        console.log(this.name + " paid invoice");
    };
    return User;
})();
var Memeber = (function (_super) {
    __extends(Memeber, _super);
    function Memeber(id, name, email, age) {
        _super.call(this, name, email, age);
        this.id = id;
    }
    Memeber.prototype.payInvoice = function () {
        _super.prototype.payInvoice.call(this);
    };
    return Memeber;
})(User);
var member = new Memeber(1, "John", "john@gmail.com", 34);
member.register();
member.payInvoice();
